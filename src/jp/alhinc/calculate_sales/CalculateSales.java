package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String SERIAL_NUMBER_ERROR = "売上ファイル名が連番になっていません";
	private static final String FORMAT_IS_INVALID = "のフォーマットが不正です";
	private static final String FILE_FORMAT_IS_INVALID = "定義ファイルのフォーマットが不正です";
	private static final String NUMBER_OF_DIGITS_ERROR = "合計金額が10桁を超えました。";
	private static final String SALES_CODE_ERROR = "の支店定義コードが不正です";
	private static final String COMMODITY_CODE_ERROR = "の商品定義コードが不正です";
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	//コマンドライン引数に数字が入っているか、引数が複数渡されていないか
	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String,String> commodityNames = new HashMap<>();
		// 商品名,売上金額を保持するMap
		Map<String,Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}$","支店")) {

			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[a-zA-Z0-9]{8}$", "商品")) {

			return;
		}

		//rcdFilesに8桁のrcdFileを格納
		File[] files =  new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		for(int i = 0; i < files.length; i++) {
			files[i].getName();
			if((files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$"))) {
				rcdFiles.add(files[i]);
			}
		}

		//売上ファイルの番号を比較し、1以上のズレが生じた場合return
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() -1; i++) {
			File rcdFile1 = rcdFiles.get(i);
			File rcdFile2 = rcdFiles.get(i + 1);
			String rcdName1 = rcdFile1.getName();
			String rcdName2 = rcdFile2.getName();
			int former = Integer.parseInt(rcdName1.substring(0, 8));
			int latter = Integer.parseInt(rcdName2.substring(0, 8));
			if((latter - former) != 1) {
				System.out.println(SERIAL_NUMBER_ERROR);
				return;
			}
		}
		BufferedReader br = null;
		try {
			for(int i = 0; i < rcdFiles.size();i++) {
				File rcdFile = rcdFiles.get(i);
				br = new BufferedReader(new FileReader(rcdFile));
				String line;
				ArrayList<String> list = new ArrayList<>();
				while((line = br.readLine()) != null) {
					list.add(line);
				}
				//商品売上ファイルに格納されている(支店コード,商品コード,商品名)の3つが入っているか
				if(list.size() != 3) {
					System.out.println(rcdFile.getName() + FORMAT_IS_INVALID);
					return;
				}
				//売上ファイルの3行目が数字であるか
				if(!list.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				String code = list.get(0);
				String commodityCode = list.get(1);
				String sales = list.get(2);

				//支店定義ファイルの「支店コード」と、売上ファイルの「支店コード」が一致しているか
				if(!branchNames.containsKey(code)) {
					System.out.println(rcdFile.getName() + SALES_CODE_ERROR);
					return;
				}

				//商品定義ファイルの「商品コード」と、商品売上ファイルの「商品コード」が一致しているか
				if(!commodityNames.containsKey(commodityCode)) {
					System.out.println(rcdFile.getName() + COMMODITY_CODE_ERROR);
					return;
				}

				//売上ファイルの合計金額
				long fileSale = Long.parseLong(sales);
				Long salesAmount = branchSales.get(code) + fileSale;

				//商品売上ファイルの合計金額
				Long commoditySalesAmount = commoditySales.get(commodityCode) + fileSale;

				//売上ファイルと商品売上ファイルのそれぞれが合計金額が10桁を超えていないか
				if((salesAmount >= 10000000000L || commoditySalesAmount >= 10000000000L)) {
					System.out.println(NUMBER_OF_DIGITS_ERROR);
					return;
				}

				//支店コード,支店売上へ反映
				branchSales.put(code, salesAmount);

				//商品コード,商品売上へ反映
				commoditySales.put(commodityCode, commoditySalesAmount);
			}

		}catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return;

		}finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}
	/**
	 * 支店定義ファイル読み込み処理
	 * 商品定義ファイル読み込み処理
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names,
			Map<String, Long> sales, String match, String definition) {
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);
			if(!file.exists()) {
				System.out.println(definition + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");

				//itemsに値が2つでないまたは支店コードが正しくない場合
				if((items.length != 2 || (!items[0].matches(match)))) {
					System.out.println(definition + FILE_FORMAT_IS_INVALID);
					return false;
				}
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);

			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names,
			Map<String, Long> sales) {
		BufferedWriter bw = null;
		try {
			File createFile = new File(path, fileName);
			FileWriter fw = new FileWriter(createFile);
			bw = new BufferedWriter(fw);
			for(String branchName : names.keySet()) {
				String branchSale = (branchName + "," + (names.get(branchName)) + "," +(sales.get(branchName)));
				bw.write(branchSale);
				bw.newLine();
			}

		}catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		}finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
